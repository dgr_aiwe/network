package com.example.network;

import android.app.Application;

import com.example.network.di.component.ActivityComponent;
import com.example.network.di.component.AppComponent;
import com.example.network.di.component.DaggerActivityComponent;
import com.example.network.di.component.DaggerAppComponent;
import com.example.network.di.module.ApplicationModule;
import com.example.network.di.module.DatabaseModule;


public class App extends Application {

    private static App app;
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        appComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .databaseModule(new DatabaseModule()).build();
        appComponent.injectTo(this);
    }

    public static App getInstance() {
        return app;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
