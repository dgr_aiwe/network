package com.example.network.config;

public class Const {
    public static final String ONE_MINUTE = "1min";
    public static final String SIXTY_MINUTES = "60min";
    public static final String FOR_MONTH = "TIME_SERIES_MONTHLY";
    public static final String FOR_DAY = "TIME_SERIES_INTRADAY";
    public static final String FOR_WEEK = "TIME_SERIES_WEEKLY";
    public static final int QUANTITY_STOCKS_FOR_LOADING_FIRST_TIME = 17;
    public static final int QUANTITY_STOCKS_FOR_LOADING_NEXT_TIME = 5;
}
