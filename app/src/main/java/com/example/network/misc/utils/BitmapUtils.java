package com.example.network.misc.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class BitmapUtils {
    public static void loadIcon(Context context, String url, ImageView place) {
        Glide.with(context)
                .load(url)
                .into(place);

    }
}
