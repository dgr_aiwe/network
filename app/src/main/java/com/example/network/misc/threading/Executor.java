package com.example.network.misc.threading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executor {

    public static void execute(Runnable runnable) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(runnable);
    }
}
