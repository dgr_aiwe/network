package com.example.network.model.POJO;

import java.util.Map;
import java.util.TreeMap;

public abstract class PriceByTime {

    public Map<String, Price> getPriceByTime() {
        if (this instanceof PriceByMinute) {
            return ((PriceByMinute) this).getPrice();
        }
        else if (this instanceof PriceByHour) {
            return ((PriceByHour) this).getPrice();
        }
        else if (this instanceof PriceByWeek) {
            return ((PriceByWeek) this).getPrice();
        }
        else if (this instanceof PriceByMonth) {
            return ((PriceByMonth) this).getPrice();
        }
        return null;
    }
}
