package com.example.network.model.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MetaData {

    @SerializedName("1. Information")
    @Expose
    private String information;
    @SerializedName("2. Symbol")
    @Expose
    private String symbol;
    @SerializedName("3. Last Refreshed")
    @Expose
    private String lastRefresh;
    @SerializedName("4. Interval")
    @Expose
    private String interval;
    @SerializedName("5. Output Size")
    @Expose
    private String outputSize;
    @SerializedName("6. Time Zone")
    @Expose
    private String timeZone;

    public String getInformation() {
        return information;
    }

    public void setInformation(String inf) {
        this.information = inf;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String sym) {
        this.symbol = sym;
    }

    public String getLastRefreshed() {
        return lastRefresh;
    }

    public void setLastRefreshed(String lastRefreshed) {
        this.lastRefresh = lastRefreshed;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getOutputSize() {
        return outputSize;
    }

    public void setOutputSize(String size) {
        this.outputSize = size;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

}
