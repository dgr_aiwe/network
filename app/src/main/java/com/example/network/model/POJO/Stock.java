package com.example.network.model.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stock {
    /*
    * POJO Class
    */

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ticker")
    @Expose
    private String ticker;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lei")
    @Expose
    private String lei;
    @SerializedName("cik")
    @Expose
    private String cik;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLei() {
        return lei;
    }

    public void setLei(String lei) {
        this.lei = lei;
    }

    public String getCik() {
        return cik;
    }

    public void setCik(String cik) {
        this.cik = cik;
    }


}
