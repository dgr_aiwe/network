package com.example.network.model.POJO;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StockList {

    @SerializedName("companies")
    @Expose
    private List<Stock> stocks = null;

    @SerializedName("next_page")
    @Expose
    private String nextPage;

    public List<Stock> getStocks() {
        return stocks;
    }

    public void setStocks(List<Stock> stocks) {
        this.stocks = stocks;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }
}
