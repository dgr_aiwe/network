package com.example.network.model.POJO;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class PriceByMonth extends PriceByTime {

    @SerializedName("Meta Data")
    private MetaData metaData;
    @SerializedName("Monthly Time Series")
    private Map<String, Price> price;

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public Map<String, Price> getPrice() {
        return price;
    }

    public void setPrice(Map<String, Price> price) {
        this.price = price;
    }

}
