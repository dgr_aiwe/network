package com.example.network.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.example.network.di.component.DaggerFragmentComponent;
import com.example.network.di.factory.DemoViewModelFactory;
import com.example.network.R;
import com.example.network.model.POJO.Company;
import com.example.network.model.POJO.CompanyProfile;
import com.example.network.ui.Navigator;
import com.example.network.ui.main.adapter.DiffCallback;
import com.example.network.ui.main.adapter.StocksRecycleAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class StockListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
    OnStockWasSelectedListener {

    public static StockListFragment getInstance() {
        return new StockListFragment();
    }

    @BindView(R.id.fslStocksRecycleView)
    RecyclerView stocksRecycleView;

    @BindView(R.id.fdProgress)
    ProgressBar progress;

    @BindView(R.id.fslSwipeRefreshLayout)
    SwipeRefreshLayout refreshLayout;

    @Inject
    Navigator navigator;

    @Inject
    DemoViewModelFactory factory;

    private StockListViewModel viewModel;
    private StocksRecycleAdapter adapter;
    private LinearLayoutManager manager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerFragmentComponent.builder()
                .activityComponent(((MainActivity) getActivity()).activityComponent)
                .build().inject(this);

        viewModel = ViewModelProviders.of(this, factory).get(StockListViewModel.class);
        viewModel.getStockList();

        adapter = new StocksRecycleAdapter();
        adapter.setListener(this);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_stock_list, container, false);
        ButterKnife.bind(this, view);

        progress.setVisibility(View.VISIBLE);

        manager = new LinearLayoutManager(getContext());
        stocksRecycleView.setAdapter(adapter);
        stocksRecycleView.setLayoutManager(manager);

        refreshLayout.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewModel.companiesLiveData.observe(getViewLifecycleOwner(), new Observer<List<Company>>() {
            @Override
            public void onChanged(List<Company> newCompanies) {
                progress.setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
                adapter.removeProgress();

                DiffCallback diffCallback = new DiffCallback(adapter.getData(), newCompanies);
                DiffUtil.DiffResult result = DiffUtil.calculateDiff(diffCallback);
                adapter.setData(newCompanies);
                result.dispatchUpdatesTo(adapter);

            }
        });
    }

    @Override
    public void onRefresh() {
        viewModel.refresh();
        adapter.removeProgress();
    }

    @Override
    public void onResume() {
        super.onResume();
        stocksRecycleView.addOnScrollListener(listener);
    }


    @Override
    public void onStop() {
        super.onStop();
        stocksRecycleView.removeOnScrollListener(listener);
    }

    private RecyclerView.OnScrollListener listener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if (viewModel.isAllowLoading()) {
                if ((manager.getItemCount() - manager.findFirstVisibleItemPosition()) <=
                        manager.findFirstVisibleItemPosition() + 5) {
                    viewModel.getExtraStocks();
                    adapter.addProgress();
                }
            }
            super.onScrolled(recyclerView, dx, dy);
        }
    };

    @Override
    public void stockWasSelected(Company company) {
        viewModel.selectedStock(company);
        navigator.openDetailFragment();
    }
}
