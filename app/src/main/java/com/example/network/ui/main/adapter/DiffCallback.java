package com.example.network.ui.main.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.example.network.model.POJO.Company;

import java.util.List;

public class DiffCallback extends DiffUtil.Callback {

    private List<Company> oldList;
    private List<Company> newList;

    public DiffCallback(List<Company> oldList, List<Company> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getId().equals(newList.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getProfile().getChangesPercentage()
                .equals(newList.get(newItemPosition).getProfile().getChangesPercentage());
    }
}
