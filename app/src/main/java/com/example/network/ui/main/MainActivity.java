package com.example.network.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.network.di.annotations.ActivityScope;
import com.example.network.di.component.ActivityComponent;
import com.example.network.R;
import com.example.network.App;
import com.example.network.di.component.DaggerActivityComponent;
import com.example.network.di.module.ActivityNavModule;
import com.example.network.ui.Navigator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

@ActivityScope
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.amTopContainer)
    FrameLayout container;

    @Inject
    Navigator navigator;

    @Inject
    public MainActivity() { }

    public ActivityComponent activityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        activityComponent = DaggerActivityComponent.builder().appComponent(App.getInstance().getAppComponent())
                .activityNavModule(new ActivityNavModule(this)).build();
        activityComponent.injectTo(this);

        navigator.setContainer(container.getId());
        navigator.openStockListFragment();
    }
}
