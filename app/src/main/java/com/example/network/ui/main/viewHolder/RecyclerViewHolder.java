package com.example.network.ui.main.viewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.network.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.isLogoImageView)
    public ImageView logo;
    @BindView(R.id.isCompanyNameTextView)
    public TextView name;
    @BindView(R.id.isStockCostTextView)
    public TextView cost;
    @BindView(R.id.isPercentOfChangeTextView)
    public TextView percentChanges;

    public RecyclerViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}