package com.example.network.ui.details;

import com.example.network.model.POJO.PriceByTime;

import java.util.Map;

public interface PriceLoadedListener {
    void loaded(Map<String, PriceByTime> map);
}
