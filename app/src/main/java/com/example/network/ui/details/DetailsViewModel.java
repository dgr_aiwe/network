package com.example.network.ui.details;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.network.config.Const;
import com.example.network.model.POJO.Price;
import com.example.network.model.POJO.PriceByHour;
import com.example.network.model.POJO.PriceByMinute;
import com.example.network.model.POJO.PriceByMonth;
import com.example.network.model.POJO.PriceByTime;
import com.example.network.domain.use_cases.StocksUseCase;
import com.example.network.model.POJO.PriceByWeek;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class DetailsViewModel extends ViewModel implements PriceLoadedListener {

    private MutableLiveData<List<LineGraphSeries<DataPoint>>> coordinateList = new MutableLiveData<>();
    public LiveData<List<LineGraphSeries<DataPoint>>> coordinateListLiveData = coordinateList;
    private StocksUseCase useCase;


    public void loadPrices(String symbol) {
        useCase.getPrices(symbol);
    }

    @Inject
    public DetailsViewModel(StocksUseCase useCase) {
        this.useCase = useCase;
        useCase.setUpListener(this);
    }

    @Override
    public void loaded(Map<String, PriceByTime> map) {
        List<PriceByTime> prices = new ArrayList<>();
        prices.add(map.get(Const.FOR_DAY + Const.ONE_MINUTE));
        prices.add(map.get(Const.FOR_DAY + Const.SIXTY_MINUTES));
        prices.add(map.get(Const.FOR_WEEK));
        prices.add(map.get(Const.FOR_MONTH));

        List<LineGraphSeries<DataPoint>> coordinates = getCoordinatesList(prices);
        coordinateList.setValue(coordinates);
    }

    private List<LineGraphSeries<DataPoint>> getCoordinatesList(List<PriceByTime> list) {

        DataPoint[] pointsForMinute = getPoints(list.get(0));
        LineGraphSeries<DataPoint> seriesForMinute = new LineGraphSeries<>(pointsForMinute);

        DataPoint[] pointsForHours = getPoints(list.get(1));
        LineGraphSeries<DataPoint> seriesForHours = new LineGraphSeries<>(pointsForHours);

        DataPoint[] pointsForWeek = getPoints(list.get(2));
        LineGraphSeries<DataPoint> seriesForWeek = new LineGraphSeries<>(pointsForWeek);

        DataPoint[] pointsForMonth = getPoints(list.get(3));
        LineGraphSeries<DataPoint> seriesForMonth = new LineGraphSeries<>(pointsForMonth);

        List<LineGraphSeries<DataPoint>> listOfSeries = new ArrayList<>();
        listOfSeries.add(seriesForMinute);
        listOfSeries.add(seriesForHours);
        listOfSeries.add(seriesForWeek);
        listOfSeries.add(seriesForMonth);

        return listOfSeries;
    }

    private DataPoint[] getPoints(PriceByTime price) {
        int count = 0;
        DataPoint[] dataPoints = new DataPoint[9];
        for (Map.Entry<String, Price> entry : price.getPriceByTime().entrySet()) {
            if (count != 9) {
                dataPoints[count] = new DataPoint(count, Double.valueOf(entry.getValue().getHigh()));
                count++;
            } else break;
        }
        return dataPoints;
    }
}
