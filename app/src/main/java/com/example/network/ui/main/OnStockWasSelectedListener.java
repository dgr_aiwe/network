package com.example.network.ui.main;

import com.example.network.model.POJO.Company;

public interface OnStockWasSelectedListener {
    void stockWasSelected(Company company);
}
