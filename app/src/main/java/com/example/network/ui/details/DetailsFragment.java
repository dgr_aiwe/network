package com.example.network.ui.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.example.network.di.component.DaggerFragmentComponent;
import com.example.network.di.factory.DemoViewModelFactory;
import com.example.network.R;
import com.example.network.ui.details.adapter.CustomAdapterViewPager;
import com.example.network.ui.main.MainActivity;
import com.example.network.misc.utils.BitmapUtils;
import com.example.network.model.POJO.Company;
import com.example.network.model.POJO.PriceByTime;
import com.example.network.ui.main.StockListViewModel;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsFragment extends Fragment {

    public static DetailsFragment getInstance() {
        return new DetailsFragment();
    }

    @Inject
    DemoViewModelFactory factory;

    private Company company;
    private DetailsViewModel viewModel;
    private CustomAdapterViewPager adapterViewPager;
    private StockListViewModel stockListViewModel;

    @BindView(R.id.isCompanyNameTextView)
    TextView companyName;

    @BindView(R.id.isLogoImageView)
    ImageView logo;

    @BindView(R.id.fdDescriptionTextView)
    TextView description;

    @BindView(R.id.fdViewPagerGraphics)
    ViewPager viewPager;

    @BindView(R.id.fdToolbar)
    Toolbar toolbar;

    @BindView(R.id.fdProgress)
    ProgressBar progress;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerFragmentComponent.builder()
                .activityComponent(((MainActivity) getActivity()).activityComponent)
                .build().inject(this);

        viewModel = ViewModelProviders.of(getActivity(), factory).get(DetailsViewModel.class);
        stockListViewModel = ViewModelProviders.of(getActivity(), factory).get(StockListViewModel.class);

        company = stockListViewModel.selectedCompanyLiveData.getValue();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        ButterKnife.bind(this, view);
        viewModel.loadPrices(company.getSymbol());

        viewPager.setOffscreenPageLimit(4);

        viewModel.coordinateListLiveData.observe(getViewLifecycleOwner(), new Observer<List<LineGraphSeries<DataPoint>>>() {
            @Override
            public void onChanged(List<LineGraphSeries<DataPoint>> lineGraphSeries) {
                adapterViewPager = new CustomAdapterViewPager(getContext());
                adapterViewPager.setDataInAdapter(lineGraphSeries);
                viewPager.setAdapter(adapterViewPager);
                viewPager.setCurrentItem(0);
                progress.setVisibility(View.GONE);
            }
        });

        toolbar.setTitle("Details");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        companyName.setText(company.getProfile().getCompanyName());
        description.setText(company.getProfile().getDescription());
        BitmapUtils.loadIcon(getContext(), company.getProfile().getImage(), logo);

        return view;
    }


}
