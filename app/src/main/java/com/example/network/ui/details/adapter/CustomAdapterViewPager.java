package com.example.network.ui.details.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.network.R;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

public class CustomAdapterViewPager extends PagerAdapter {
    private Context context;
    private GraphView graphView;
    private ViewGroup layout;
    private TextView title;
    private List<LineGraphSeries<DataPoint>> coordinateList;

    public CustomAdapterViewPager(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return coordinateList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater inflater = LayoutInflater.from(context);
        layout = (ViewGroup) inflater.inflate(R.layout.item_stock_price_detalize, container, false);
        graphView = layout.findViewById(R.id.ispdGraphic);
        title = layout.findViewById(R.id.itodTitleTimeTextView);

        switch (position) {
            case 0:
                title.setText("By minutes");
                graphView.addSeries(coordinateList.get(0));
                break;
            case 1:
                graphView.addSeries(coordinateList.get(1));
                title.setText("By hours");
                break;
            case 2:
                graphView.addSeries(coordinateList.get(2));
                title.setText("By weeks");
                break;
             case 3:
                 graphView.addSeries(coordinateList.get(3));
                 title.setText("By months");
                 break;
            }
        container.addView(layout);
        return layout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public void setDataInAdapter(List<LineGraphSeries<DataPoint>> list) {
        coordinateList = list;
        this.notifyDataSetChanged();
    }
}
