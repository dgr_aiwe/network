package com.example.network.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.network.config.Const;
import com.example.network.model.POJO.Company;
import com.example.network.domain.use_cases.StocksUseCase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class StockListViewModel extends ViewModel  {

    private StocksUseCase useCase;

    private MutableLiveData<Company> selectedCompany = new MutableLiveData<>();
    public LiveData<Company> selectedCompanyLiveData = selectedCompany;
    private MutableLiveData<List<Company>> companies = new MutableLiveData<>();
    public LiveData<List<Company>> companiesLiveData = companies;
    private List<Company> companyList = new ArrayList<>();
    private boolean allowLoading = false;

    public boolean isAllowLoading() {
        return allowLoading;
    }

    @Inject
    public StockListViewModel(StocksUseCase useCase) {
        this.useCase = useCase;
    }

    public void selectedStock(Company company) {
        selectedCompany.setValue(company);
    }

    public void getStockList() {
        useCase.companyList.subscribe(new Observer<List<Company>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<Company> loadedList) {
                if (loadedList.size() >= Const.QUANTITY_STOCKS_FOR_LOADING_FIRST_TIME) {
                    allowLoading = true;
                    companyList.clear();
                }
                else {
                    allowLoading = (loadedList.size() == Const.QUANTITY_STOCKS_FOR_LOADING_NEXT_TIME);
                }

                companyList.addAll(loadedList);
                companies.postValue(companyList);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
        useCase.getStockList();
    }
    public void getExtraStocks() {
        allowLoading = false;
        useCase.loadExtraStocks();
    }

    public void refresh() {
        useCase.refresh();
    }
}
