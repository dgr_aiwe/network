package com.example.network.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.network.ui.details.DetailsFragment;
import com.example.network.ui.main.MainActivity;
import com.example.network.ui.main.StockListFragment;

import javax.inject.Inject;


public class Navigator {

    private DetailsFragment detailsFragment;
    private StockListFragment stockListFragment;
    private AppCompatActivity activity;

    private int containerId;

    public void setContainer(int id) {
        containerId = id;
    }

    @Inject
    public Navigator(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void openStockListFragment() {
        stockListFragment = StockListFragment.getInstance();
        doTransactionWithoutBackStack(stockListFragment);
    }

    public void openDetailFragment() {
        detailsFragment = DetailsFragment.getInstance();
        doTransactionWithBackStack(detailsFragment);
    }

    private void doTransactionWithBackStack(Fragment fragment) {
        activity.getSupportFragmentManager().beginTransaction()
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void doTransactionWithoutBackStack(Fragment fragment) {
        activity.getSupportFragmentManager().beginTransaction()
                .replace(containerId, fragment)
                .commit();
    }
}
