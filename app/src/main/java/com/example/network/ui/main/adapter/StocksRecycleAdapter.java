package com.example.network.ui.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.network.R;
import com.example.network.ui.main.OnStockWasSelectedListener;
import com.example.network.misc.utils.BitmapUtils;
import com.example.network.model.POJO.Company;
import com.example.network.ui.main.viewHolder.ProgressViewHolder;
import com.example.network.ui.main.viewHolder.RecyclerViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class StocksRecycleAdapter extends RecyclerView.Adapter {
    private List<Company> adapterList = new ArrayList<>();
    private OnStockWasSelectedListener listener;
    private static final int VIEW_ITEM = 1;
    private static final int VIEW_PROG = 2;

    public void setListener(OnStockWasSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return adapterList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public List<Company> getData() {
        return adapterList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        if (viewType == VIEW_ITEM) {
            viewHolder = createCompanyItem(parent);
        }
        else {
            viewHolder = createProgressItem(parent);
        }

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RecyclerViewHolder && adapterList.get(position) != null) {

            ((RecyclerViewHolder) holder).name.setText(adapterList.get(position).getProfile().getCompanyName());
            ((RecyclerViewHolder) holder).cost.setText(adapterList.get(position).getProfile().getPrice().toString());
            ((RecyclerViewHolder) holder).percentChanges.setText(adapterList.get(position).getProfile().getChangesPercentage()
                    .replaceAll("[()]", ""));

            BitmapUtils.loadIcon(holder.itemView.getContext(), adapterList.get(position).getProfile().getImage(),
                    ((RecyclerViewHolder) holder).logo);

            holder.itemView.setOnClickListener((v) -> {
                listener.stockWasSelected(adapterList.get(position));
            });
        }
        else {
            ((ProgressViewHolder) holder).progress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (adapterList != null) {
            return adapterList.size();
        }
        return 0;
    }

    @NotNull
    private RecyclerView.ViewHolder createProgressItem(@NonNull ViewGroup parent) {
        RecyclerView.ViewHolder viewHolder;
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_progress, parent, false);
        viewHolder = new ProgressViewHolder(view);
        return viewHolder;
    }

    @NotNull
    private RecyclerView.ViewHolder createCompanyItem(@NonNull ViewGroup parent) {
        RecyclerView.ViewHolder viewHolder;
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_stock, parent, false);
        viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    public void setData(List<Company> list) {
        adapterList.clear();
        adapterList.addAll(list);
    }

    public void addProgress() {
        adapterList.add(null);
        this.notifyItemInserted(adapterList.size() - 1);
    }

    public void removeProgress() {
        this.notifyItemRemoved(adapterList.size() - 1);
        adapterList.remove(null);
    }
}
