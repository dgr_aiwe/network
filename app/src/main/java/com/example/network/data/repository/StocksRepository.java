package com.example.network.data.repository;

import android.util.Log;

import com.example.network.data.network.StockService;
import com.example.network.model.POJO.PriceByHour;
import com.example.network.model.POJO.PriceByMinute;
import com.example.network.model.POJO.PriceByMonth;
import com.example.network.model.POJO.PriceByTime;
import com.example.network.model.POJO.PriceByWeek;
import com.example.network.model.POJO.Stock;
import com.example.network.model.POJO.StockList;
import com.example.network.data.db.database.AppDatabase;
import com.example.network.model.POJO.Company;
import com.example.network.config.Const;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StocksRepository {
    private List<Company> loadedStocks = new ArrayList<>();
    private String nextPageReference;
    private List<Stock> savedStocks = new ArrayList<>();
    private int countUnsavedStocks = 0;
    private int count = 0;
    private static final String API_KEY = "OjkyYmU3NzI1ZjA3NTg0NGRjNjE3YzA5ZTgwN2IwZWZl";
    private static final String PRICE_IN_TIME_URL = "https://www.alphavantage.co/query";
    private static final String DETAILS_URL = "https://financialmodelingprep.com/api/v3/company/profile/";

    private static final int QUANTITY_GRAPHICS = 4;

    private Map<String, PriceByTime> prices = new HashMap<>();
    private AppDatabase database;
    private StockService stocksApi;
    private int quantitySavedCompanies = 0;

    public PublishSubject<List<Company>> companyList = PublishSubject.create();
    public PublishSubject<Map<String, PriceByTime>> priceSubject = PublishSubject.create();

    @Inject
    public StocksRepository(AppDatabase database, StockService stocksService) {
        this.database = database;
        stocksApi = stocksService;
    }

    private Consumer<List<Company>> observer = new Consumer<List<Company>>() {
        @Override
        public void accept(List<Company> companies) throws Exception {
            companyList.onNext(companies);
            quantitySavedCompanies = companies.size();
        }
    };

    public void getStockList() {

        database.companyProfileDao().getCompanies(Const.QUANTITY_STOCKS_FOR_LOADING_FIRST_TIME, 0)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

        if (quantitySavedCompanies != 0) {
            loadStocksFromNetworkWithQuantity(quantitySavedCompanies);
        }
        else {
            loadStocksFromNetworkWithQuantity(Const.QUANTITY_STOCKS_FOR_LOADING_FIRST_TIME);
        }

    }

    private ResponseStatus responseStatus = new ResponseStatus() {
        @Override
        public void succeed(List<Company> profiles) {
            database.companyProfileDao().insert(profiles);
        }

        @Override
        public void failed() {
            companyList.onNext(Collections.emptyList());
        }

        @Override
        public void alreadyLoaded() {
            companyList.onNext(Collections.emptyList());
        }
    };

    private PriceLoadStatus priceLoadStatus = new PriceLoadStatus() {
        @Override
        public void succeed(Map<String, PriceByTime> data) {
             priceSubject.onNext(prices);
        }

        @Override
        public void failed() {

        }
    };


    public void loadExtraStocks() {
        clearLoadedList();
        loadStocksFromNetworkWithQuantity(Const.QUANTITY_STOCKS_FOR_LOADING_NEXT_TIME);
    }

    public void onRefresh() {
        clearAllLists();
        loadStocksFromNetworkWithQuantity(Const.QUANTITY_STOCKS_FOR_LOADING_FIRST_TIME);
    }

    private void loadStocksFromNetworkWithQuantity(int quantity) {

        stocksApi.getMoreStocks(API_KEY, quantity, nextPageReference)
                .enqueue(new Callback<StockList>() {

                    @Override
                    public void onResponse(Call<StockList> call, Response<StockList> response) {
                        if (response.body() != null) {
                            countUnsavedStocks = 0;
                            checkStocksAlreadyLoaded(response);

                            if (countUnsavedStocks == quantity) {
                                nextPageReference = response.body().getNextPage();
                                loadDetailsByQuantity(response.body().getStocks());
                            }
                            else if (countUnsavedStocks != 0){
                                loadStocksFromNetworkWithQuantity(countUnsavedStocks);
                            }
                            else {
                                responseStatus.alreadyLoaded();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<StockList> call, Throwable t) {
                        responseStatus.failed();
                        Log.d("Debug", "onFailure: " + t.getMessage());
                    }
                });
    }

    private void checkStocksAlreadyLoaded(Response<StockList> response) {
        for (Stock stock : response.body().getStocks()) {
            if (!savedStocks.contains(stock)) {
                countUnsavedStocks++;
                savedStocks.add(stock);
            }
        }
    }


    private void loadDetailsByQuantity(List<Stock> list) {
        for (Stock stock: list) {

            stocksApi.getDetails(DETAILS_URL + stock.getTicker())
                    .enqueue(new Callback<Company>() {

                        @Override
                        public void onResponse(Call<Company> call, Response<Company> response) {
                            if (response.body() != null) {

                                Company company = response.body();
                                company.setId(company.getSymbol());

                                loadedStocks.add(company);
                                count++;

                                if (count == list.size()) {
                                    responseStatus.succeed(loadedStocks);
                                    count = 0;
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Company> call, Throwable t) {
                            responseStatus.failed();
                            Log.d("Debug", "onFailure: " + t.getMessage());
                        }
                    });
        }
    }

    public void getPrices(String symbol) {
        prices.clear();
        getResponseForMinutes(symbol);
        getResponseForHours(symbol);
        getResponseForWeek(symbol);
        getResponseForMonth(symbol);
    }

    private void getResponseForMinutes(String symbol) {
        stocksApi.getPriceInMinutes(PRICE_IN_TIME_URL, Const.FOR_DAY, symbol,
                Const.ONE_MINUTE, "1M0ILJ2KQ08IFMK").enqueue(new Callback<PriceByMinute>() {

            @Override
            public void onResponse(Call<PriceByMinute> call, Response<PriceByMinute> response) {
                synchronized (prices) {
                    prices.put(Const.FOR_DAY + Const.ONE_MINUTE, response.body());
                }
                if (prices.size() == QUANTITY_GRAPHICS) {
                    priceLoadStatus.succeed(prices);
                }

                if (response.body().getPrice() != null) {
                    Log.d("Debug", "Minutes response - " + response.body().getPrice().size());
                }
                else {
                    Log.d("Debug", "Minutes response - null");
                }
            }

            @Override
            public void onFailure(Call<PriceByMinute> call, Throwable t) {
                priceLoadStatus.failed();
            }
        });
    }

    private void getResponseForHours(String symbol) {
        stocksApi.getPriceInHours(PRICE_IN_TIME_URL, Const.FOR_DAY, symbol,
                Const.SIXTY_MINUTES, "1M0ILJ2KQ08IFMK").enqueue(new Callback<PriceByHour>() {

            @Override
            public void onResponse(Call<PriceByHour> call, Response<PriceByHour> response) {
                synchronized (prices) {
                    prices.put(Const.FOR_DAY + Const.SIXTY_MINUTES, response.body());
                }
                if (prices.size() == QUANTITY_GRAPHICS) {
                    priceLoadStatus.succeed(prices);
                }
                if (response.body().getPrice() != null) {
                    Log.d("Debug", "Hours response - " + response.body().getPrice().size());
                }
                else {
                    Log.d("Debug", "Hours response - null");
                }
            }

            @Override
            public void onFailure(Call<PriceByHour> call, Throwable t) {
                priceLoadStatus.failed();
            }
        });
    }

    private void getResponseForWeek(String symbol) {
        stocksApi.getPriceInWeeks(PRICE_IN_TIME_URL, Const.FOR_WEEK, symbol,
                "1M0ILJ2KQ08IFMK").enqueue(new Callback<PriceByWeek>() {

            @Override
            public void onResponse(Call<PriceByWeek> call, Response<PriceByWeek> response) {
                synchronized (prices) {
                    prices.put(Const.FOR_WEEK, response.body());
                }

                if (prices.size() == QUANTITY_GRAPHICS) {
                    priceLoadStatus.succeed(prices);
                }
                if (response.body().getPrice() != null) {
                    Log.d("Debug", "Weeks response - " + response.body().getPrice().size());
                }
                else {
                    Log.d("Debug", "Weeks response - null");
                }
            }

            @Override
            public void onFailure(Call<PriceByWeek> call, Throwable t) {
                priceLoadStatus.failed();
            }
        });
    }

    private void getResponseForMonth(String symbol) {
        stocksApi.getPriceInMonth(PRICE_IN_TIME_URL, Const.FOR_MONTH, symbol,
                "1M0ILJ2KQ08IFMK").enqueue(new Callback<PriceByMonth>() {

            @Override
            public void onResponse(Call<PriceByMonth> call, Response<PriceByMonth> response) {
                synchronized (prices) {
                    prices.put(Const.FOR_MONTH, response.body());
                }

                if (prices.size() == QUANTITY_GRAPHICS) {
                    priceLoadStatus.succeed(prices);
                }

                if (response.body().getPrice() != null) {
                    Log.d("Debug", "Months response - " + response.body().getPrice().size());
                }
                else {
                    Log.d("Debug", "Months response - null");
                }

            }

            @Override
            public void onFailure(Call<PriceByMonth> call, Throwable t) {
                priceLoadStatus.failed();
            }
        });
    }


    public void clearAllLists() {
        savedStocks.clear();
        loadedStocks.clear();
        nextPageReference = "";
    }

    public void clearLoadedList() {
        loadedStocks.clear();
    }
}