package com.example.network.data.network;

import com.example.network.model.POJO.Company;
import com.example.network.model.POJO.PriceByHour;
import com.example.network.model.POJO.PriceByMinute;
import com.example.network.model.POJO.PriceByMonth;
import com.example.network.model.POJO.PriceByWeek;
import com.example.network.model.POJO.StockList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface StockService {

    @GET("companies")
    Call<StockList> getStocks(@Query("api_key") String api_key, @Query("page_size") int page_size);

    @GET("companies")
    Call<StockList> getMoreStocks(@Query("api_key") String api_key, @Query("page_size") int page_size,
                                  @Query("next_page") String next_page);

    @GET
    Call<PriceByMinute> getPriceInMinutes(@Url String url, @Query("function") String time, @Query("symbol") String symbol,
                                          @Query("interval") String interval, @Query("apikey")String api_key);

    @GET
    Call<PriceByHour> getPriceInHours(@Url String url, @Query("function") String time, @Query("symbol") String symbol,
                                      @Query("interval") String interval, @Query("apikey")String api_key);

    @GET
    Call<PriceByWeek> getPriceInWeeks(@Url String url, @Query("function") String time, @Query("symbol") String symbol,
                                      @Query("apikey")String api_key);

    @GET
    Call<PriceByMonth> getPriceInMonth(@Url String url, @Query("function") String time, @Query("symbol") String symbol,
                                       @Query("apikey")String api_key);

    @GET()
    Call<Company> getDetails(@Url String url);
}

