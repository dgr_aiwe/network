package com.example.network.data.repository;

import com.example.network.model.POJO.PriceByTime;

import java.util.Map;

public interface PriceLoadStatus {
    void succeed(Map<String, PriceByTime> data);
    void failed();
}
