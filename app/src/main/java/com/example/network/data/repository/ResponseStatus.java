package com.example.network.data.repository;

import com.example.network.model.POJO.Company;

import java.util.List;

public interface ResponseStatus {
    void succeed(List<Company> profiles);
    void failed();
    void alreadyLoaded();
}
