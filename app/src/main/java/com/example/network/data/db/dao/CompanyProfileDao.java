package com.example.network.data.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.network.model.POJO.Company;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface CompanyProfileDao {

    @Query("SELECT * FROM Company limit :lim offset :offset")
    Flowable<List<Company>> getCompanies(int lim, int offset);

    @Query("SELECT * FROM Company WHERE symbol = :sym")
    Company getBySymbol(String sym);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Company> list);

    @Update()
    void update(List<Company> list);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Company profile);

    @Update
    void update(Company profile);

    @Delete
    void delete(Company profile);
}
