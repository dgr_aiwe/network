package com.example.network.data.db.database;

import androidx.room.RoomDatabase;

import com.example.network.data.db.dao.CompanyProfileDao;
import com.example.network.model.POJO.Company;

@androidx.room.Database(entities = {Company.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CompanyProfileDao companyProfileDao();
}
