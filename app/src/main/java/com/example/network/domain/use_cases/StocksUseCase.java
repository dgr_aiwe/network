package com.example.network.domain.use_cases;

import android.util.Log;

import com.example.network.data.repository.StocksRepository;
import com.example.network.model.POJO.Company;
import com.example.network.di.annotations.ActivityScope;
import com.example.network.ui.details.PriceLoadedListener;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.subjects.PublishSubject;


@ActivityScope
public class StocksUseCase {

    private StocksRepository repository;
    private PriceLoadedListener listener;

    @Inject
    public StocksUseCase(StocksRepository repository) {
        this.repository = repository;
    }

    public PublishSubject<List<Company>> companyList = PublishSubject.create();

    public void setUpListener(PriceLoadedListener listener) {
        this.listener = listener;
    }

    public void getStockList() {
        repository.companyList.subscribe(companyList);
        repository.getStockList();
    }


    public void loadExtraStocks() {
        repository.loadExtraStocks();
    }

    public void refresh() {
        repository.onRefresh();
    }

    public void getPrices(String symbol) {
        repository.getPrices(symbol);
        repository.priceSubject.subscribe(map -> listener.loaded(map));
    }
}