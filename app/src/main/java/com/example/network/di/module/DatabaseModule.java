package com.example.network.di.module;

import android.content.Context;

import androidx.room.Room;

import com.example.network.data.db.database.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApplicationModule.class})
public class DatabaseModule {

    @Provides
    @Singleton
    AppDatabase provideDatabase(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "app_database")
                .allowMainThreadQueries()
                .build();
    }
}
