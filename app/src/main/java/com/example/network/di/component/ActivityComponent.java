package com.example.network.di.component;

import androidx.fragment.app.Fragment;

import com.example.network.di.annotations.ActivityScope;
import com.example.network.di.annotations.FragmentScope;
import com.example.network.di.factory.DemoViewModelFactory;
import com.example.network.di.module.ActivityNavModule;
import com.example.network.di.module.FragmentModule;
import com.example.network.di.module.ViewModelModule;
import com.example.network.ui.Navigator;
import com.example.network.ui.main.MainActivity;

import dagger.Component;

@Component(modules = {ActivityNavModule.class, ViewModelModule.class}, dependencies = {AppComponent.class})
@ActivityScope
public interface ActivityComponent {
    Navigator navigator();
    DemoViewModelFactory factory();

    void injectTo(MainActivity mainActivity);
}
