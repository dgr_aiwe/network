package com.example.network.di.module;

import com.example.network.di.annotations.ActivityScope;
import com.example.network.ui.Navigator;
import com.example.network.ui.main.MainActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityNavModule {

    MainActivity activity;

    public ActivityNavModule(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityScope
    public Navigator provideNavigatorToActivity() {
        return new Navigator(activity);
    }
}
