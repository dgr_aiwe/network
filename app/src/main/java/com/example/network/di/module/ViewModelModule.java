package com.example.network.di.module;

import androidx.lifecycle.ViewModel;

import com.example.network.di.annotations.ActivityScope;
import com.example.network.di.annotations.ViewModelKey;
import com.example.network.ui.details.DetailsViewModel;
import com.example.network.ui.main.StockListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ActivityScope
    @ViewModelKey(StockListViewModel.class)
    abstract ViewModel stockListViewModel(StockListViewModel stockListViewModel);

    @Binds
    @IntoMap
    @ActivityScope
    @ViewModelKey(DetailsViewModel.class)
    abstract ViewModel detailsViewModel(DetailsViewModel detailsViewModel);
}

