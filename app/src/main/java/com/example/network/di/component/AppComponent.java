package com.example.network.di.component;

import com.example.network.data.db.database.AppDatabase;
import com.example.network.data.network.StockService;
import com.example.network.di.module.ActivityNavModule;
import com.example.network.di.module.ApplicationModule;
import com.example.network.di.module.DatabaseModule;
import com.example.network.App;
import com.example.network.data.repository.StocksRepository;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, DatabaseModule.class})
public interface AppComponent {
    AppDatabase database();
    StockService service();

    void injectTo(App app);
}
