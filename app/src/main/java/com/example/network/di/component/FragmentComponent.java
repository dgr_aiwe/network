package com.example.network.di.component;

import com.example.network.di.annotations.FragmentScope;
import com.example.network.di.module.FragmentModule;
import com.example.network.ui.details.DetailsFragment;
import com.example.network.ui.main.StockListFragment;

import dagger.Component;
import dagger.Subcomponent;

@Component(dependencies = {ActivityComponent.class}, modules = {FragmentModule.class})
@FragmentScope
public interface FragmentComponent {

    void inject(StockListFragment fragment);
    void inject(DetailsFragment fragment);
}
